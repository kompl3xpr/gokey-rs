use std::fs::read_to_string;
use std::io::stdin;
use std::path::PathBuf;

use clap::{Args, Parser, ValueEnum};
use gokey::{Key, Seed};

#[derive(Parser)]
struct Cli {
    #[command(flatten)]
    password: MasterPassword,

    #[arg(short)]
    output_path: Option<PathBuf>,

    #[arg(short, default_value = "pass")]
    task: Task,
}

#[derive(Args)]
#[group(required = false, multiple = false)]
struct MasterPassword {
    #[arg(short = 'P')]
    password_path: Option<PathBuf>,

    #[arg(short)]
    password: Option<String>,
}

#[derive(Copy, Clone, ValueEnum)]
enum Task {
    Pass,
    Raw,
    Seed,
}

fn main() {
    let cli = Cli::parse();

    let mut writer = cli.output_path.clone().map_or_else(
        || Box::new(std::io::stdout()) as Box<dyn std::io::Write>,
        |path| Box::new(std::fs::File::create(path).unwrap()),
    );

    let key = &cli.password;
    let key = match (&key.password_path, &key.password) {
        (Some(path), _) => read_to_string(path).ok(),
        (_, passwd) => passwd.clone(),
    }
    .map(Key::from);

    let key_or_get_interactively = move || match key {
        Some(key) => key,
        _ => get_key_from_input(),
    };

    match &cli.task {
        Task::Pass => {
            todo!("pass")
        }
        Task::Raw => {
            todo!("raw")
        }
        Task::Seed => {
            if cli.output_path.is_none() {
                panic!("Generated seed should be written to a file");
            }

            writer
                .write_all(Seed::from_key(key_or_get_interactively()).as_ref())
                .unwrap()
        }
    };
}

fn get_key_from_input() -> Key {
    let mut buf = String::new();
    for _ in 0..3 {
        'from_path: for _ in 0..3 {
            println!("from path(y/n): ");
            buf.clear();
            stdin().read_line(&mut buf).unwrap();
            match buf.trim() {
                "y" | "Y" => {
                    println!("path: ");
                    stdin().read_line(&mut buf).unwrap();
                    return Key::from(read_to_string(buf).unwrap());
                }
                "n" | "N" => break 'from_path,
                _ => (),
            }
        }
        'from_str: for _ in 0..3 {
            println!("from input(y/n): ");
            buf.clear();
            stdin().read_line(&mut buf).unwrap();
            match buf.trim() {
                "y" | "Y" => {
                    println!("key: ");
                    stdin().read_line(&mut buf).unwrap();
                    return Key::from(buf);
                }
                "n" | "N" => break 'from_str,
                _ => (),
            }
        }
    }
    panic!()
}
