use aes_gcm::{
    aead::{Aead, KeyInit},
    Aes256Gcm, Nonce,
};
use pbkdf2::pbkdf2_hmac_array;
use rand::RngCore;
use sha2::Sha256;

pub struct Key {
    inner: String,
}

impl AsRef<[u8]> for Key {
    fn as_ref(&self) -> &[u8] {
        self.inner.as_ref()
    }
}

impl<S: Into<String>> From<S> for Key {
    fn from(s: S) -> Self {
        Self { inner: s.into() }
    }
}

pub struct Identifier {
    inner: String,
}

impl AsRef<[u8]> for Identifier {
    fn as_ref(&self) -> &[u8] {
        self.inner.as_ref()
    }
}

pub struct Seed {
    inner: [u8; 256],
}

impl Seed {
    pub fn from_key(key: Key) -> Seed {
        let mut rng = rand::thread_rng();
        let mut seed = [0; 256];

        rng.fill_bytes(&mut seed);

        let query = Query::from_key_and_salt(key, &seed[..12]);

        let cipher = Aes256Gcm::new_from_slice(query.as_ref()).unwrap();
        let ciphertext = cipher
            .encrypt(<&Nonce<_>>::from(&seed[..12]), &seed[12..seed.len() - 16])
            .unwrap();

        let mut vec = Vec::with_capacity(256);
        vec.extend_from_slice(&seed[..12]);
        vec.extend_from_slice(&ciphertext);

        Seed {
            inner: <[u8; 256]>::try_from(vec).unwrap(),
        }
    }
}

impl AsRef<[u8]> for Seed {
    fn as_ref(&self) -> &[u8] {
        &self.inner
    }
}

pub struct Query {
    inner: [u8; 32],
}

impl Query {
    pub fn from_key_and_identifier(key: Key, identifier: Identifier) -> Query {
        Query::from_password_and_salt(key.as_ref(), identifier.as_ref())
    }

    pub fn from_key_and_salt(key: Key, salt: &[u8]) -> Query {
        Query::from_password_and_salt(key.as_ref(), salt)
    }

    fn from_password_and_salt(password: &[u8], salt: &[u8]) -> Query {
        Query {
            inner: pbkdf2_hmac_array::<Sha256, 32>(password, salt, 4096),
        }
    }
}

impl AsRef<[u8]> for Query {
    fn as_ref(&self) -> &[u8] {
        &self.inner
    }
}

/*
impl<const N: usize> From<[u8; N]> for Query {
    fn from(s: [u8; N]) -> Self {
        Self { vec: Vec::from(s) }
    }
}
*/

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}
